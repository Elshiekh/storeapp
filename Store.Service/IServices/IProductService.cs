﻿using Store.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Service.IServices
{
   public interface IProductService
    {
        List<CatigoryViewModel> getAllCatigories();
        List<ProductViewModel> GetAllProducts();
        bool Add(ProductViewModel product);
        bool Delete(int id);
    }
}
