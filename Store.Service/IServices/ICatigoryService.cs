﻿using Store.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Service.IServices
{
   public interface ICatigoryService
    {
        List<CatigoryViewModel> getCatigories();
        bool Add(CatigoryViewModel catigory);
        bool EditCatigory(CatigoryViewModel catigory);
         bool Delete(int id);
        CatigoryViewModel getCatigoryById(int Id);
    }
}
