﻿using Store.Data;
using Store.Data.Models;
using Store.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Service
{
    public interface IUserService
    {

        IQueryable<User> GetUsers();
        User GetUser(long id);
        //bool AddUser(UserViewModel user);
        void UpdateUser(User user);
        void DeleteUser(long id);

    }
}
