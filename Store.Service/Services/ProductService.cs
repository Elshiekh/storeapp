﻿using AutoMapper;
using Store.Data.Models;
using Store.Data.ViewModels;
using Store.Repo;
using Store.Service.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Service.Services
{
   public class ProductService : IProductService
    {

        private readonly ApplicationContext dbContext;

        public ProductService(ApplicationContext _dbContext)
        {
            this.dbContext = _dbContext;
        }



        public List<CatigoryViewModel> getAllCatigories()
        {
            List<CatigoryViewModel> Catigories = new List<CatigoryViewModel>();
            var data = dbContext.Catigories;
            Catigories = Mapper.Map<List<CatigoryViewModel>>(data);
            return Catigories;
        }

        public List<ProductViewModel> GetAllProducts()
        {

            List<ProductViewModel> products = new List<ProductViewModel>();
            var model = dbContext.Products;
            products = Mapper.Map<List<ProductViewModel>>(model);
            return products;
        }

        public bool Add(ProductViewModel product)
        {          
                var data = Mapper.Map<ProductModel>(product);
            data.Availability = bool.Parse("True");
                dbContext.Products.Add(data);
                var Success = dbContext.SaveChanges();
                if (Success > 0)
                {
                    return true;
                }
                return false;           
        }

        public bool Delete(int id)
        {
            var model = dbContext.Products.Find(id);
            if (model != null)
            {
                dbContext.Remove(model);
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
