﻿using Store.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using Store.Data.ViewModels;
using Store.Data.Models;
using AutoMapper;
using Store.Service.IServices;
using System.Transactions;
using Microsoft.EntityFrameworkCore;

namespace Store.Service.Services
{
   public class CatigoryService : ICatigoryService
    {

        private readonly  ApplicationContext dbContext ;

        public CatigoryService(ApplicationContext _dbContext) {
           this.dbContext = _dbContext; 
        }
      

       public List<CatigoryViewModel> getCatigories() {

            List<CatigoryViewModel> catigories = new List<CatigoryViewModel>();
            var model = dbContext.Catigories;
            catigories = Mapper.Map<List<CatigoryViewModel>>(model);
            return catigories;
        }

        public CatigoryViewModel getCatigoryById(int Id)
        {
            CatigoryViewModel catigories = new CatigoryViewModel();
            var model = dbContext.Catigories.Find(Id);
            if (model!= null)
            {
            
            catigories = Mapper.Map<CatigoryViewModel>(model);
            return catigories;
            }
            return null;
        }

        public bool EditCatigory(CatigoryViewModel model)
        {
            var Catigory = getCatigoryById(model.ID);
            using (TransactionScope scope = new TransactionScope())
            {
                
                Catigory.EnName = model.EnName;
                Catigory.ArName = model.ArName;
            }
            var data = Mapper.Map<CatigoryModel>(Catigory);
            dbContext.Entry(data).State = EntityState.Modified;
          var Success=  dbContext.SaveChanges();
            if (Success ==1)
            {
                return true;
            }
            return false;
        }
        public  bool Add(CatigoryViewModel catigory)
        {
            var data = Mapper.Map<CatigoryModel>(catigory);
            dbContext.Catigories.Add(data);           
            var Success =  dbContext.SaveChanges();
            if (Success > 0)
            {
                return true;
            }
            return false;
        }


        public bool Delete(int id)
        {
            var model = dbContext.Catigories.Find(id);
            if (model!=null)
            {
                dbContext.Remove(model);
                dbContext.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
