﻿using Microsoft.AspNetCore.Identity;
using Store.Data;
using Store.Data.Models;
using Store.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Service
{
   public  class UserService : IUserService
    {
        private readonly ApplicationContext _context;
        private readonly UserManager<User> _userManager;
        public UserService(ApplicationContext context)
        {
            this._context = context;
        }

        public IQueryable<User> GetUsers()
        {
            return _context.Users;
        }

        public User GetUser(string id)
        {
            var user = _context.Users.Find(id);
            return user;
        }

        public User GetUser(long id)
        {
            throw new NotImplementedException();
        }
       
       // public  bool  AddUser(UserViewModel userviewmodel)
       //{
       //     var usermodel = new User { UserName = userviewmodel.Name, Email = userviewmodel.Email };
       //     var result =  _userManager.CreateAsync(usermodel, userviewmodel.Password);
       //     if (result !=null)
       //     {
       //         // Add a user to the default role, or any role you prefer here
       //         // await _userManager.AddToRoleAsync(user, "Doctor");
       //         return true;
       //     }
       //     else
       //     {
       //         return false;
       //     }

       // }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(long id)
        {
            throw new NotImplementedException();
        }

        //public void AddUser(User user)
        //{
        //    _context.(user);
        //}
        //public void UpdateUser(User user)
        //{
        //    userRepository.Update(user);
        //}

        //public void DeleteUser(long id)
        //{
        //    User user = GetUser(id);
        //    userRepository.Remove(user);
        //    userRepository.SaveChanges();
        //}
    }
}
