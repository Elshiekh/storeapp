﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Data.Models
{
    public class ProductModel
    {
        public int ID { get; set; }
        public string EnName { get; set; }
        public string ArName { get; set; }
        public double Price { get; set; }
        public int Quantaty { get; set; }
        public bool Availability { get; set; }
        public string Description { get; set; }
        public int CatogeryId { get; set; }
        public virtual CatigoryModel Catogery { get; set; }
        //public SelectList Categorylist { get; set; }
    }
}
