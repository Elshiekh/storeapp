﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Store.Data.ViewModels
{
   public class CatigoryViewModel
    {
        public int ID { get; set; }
        [Required]
        public string EnName { get; set; }
        [Required]
        public string ArName { get; set; }
    }
}
