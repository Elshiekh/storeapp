﻿using AutoMapper;
using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Store.Data.ViewModels;

namespace Store.Data.AutoMapper
{
    public class AutoMapperProfile :Profile
    {

        public AutoMapperProfile()
        {
           
            CreateMap<CatigoryModel, CatigoryViewModel>();
            CreateMap<CatigoryViewModel, CatigoryModel >();

            CreateMap<ProductModel, ProductViewModel>();
            CreateMap<ProductViewModel, ProductModel>();

        }
    }
}
