﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Data.ViewModels;
using Store.Service.IServices;

namespace Store.Web.Controllers
{
    public class ProductController : Controller
    {
      

        private readonly IProductService Service;

        public ProductController(IProductService _service)
        {
            this.Service = _service;
        }

        public IActionResult Index()
        {
            var productlist = Service.GetAllProducts();
            return View(productlist);
        }


        [HttpGet]
        public IActionResult Add()
        {
            ProductViewModel model = new ProductViewModel();
            var Categories = Service.getAllCatigories();
            model.Categorylist = new SelectList(Categories, "ID", "EnName");
            return View(model);
        }

        [HttpPost]
        public IActionResult Add(ProductViewModel data)
        {
            if (ModelState.IsValid)
            {
                bool success = Service.Add(data);
                if (success)
                {
                    return RedirectToAction("Index");
                }
            }

            return View();
        }

        public IActionResult Delete(int ID)
        {
            if (ID > 0)
            {
                var selected = Service.Delete(ID);
            }
            return RedirectToAction("Index");
        }
    }
}