﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Store.Data.ViewModels;
using Store.Service.IServices;
using Store.Service.Services;

namespace Store.Web.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICatigoryService Service ;

        public CategoriesController(ICatigoryService _Service)
        {
            this.Service = _Service;
        }

        public IActionResult Index()
        {        
            ViewBag.Catogeries  = Service.getCatigories();
            return View();
        }


        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(CatigoryViewModel data)
        {
            if (ModelState.IsValid)
            {
                bool success = Service.Add(data);
                if (success)
                {
                    return RedirectToAction("Index");
                }
            }
           
            return RedirectToAction("Index");
        }
       
        public IActionResult Delete(int ID)
        {
            if (ID>0)
            {
                var selected =  Service.Delete(ID);
            }
            return RedirectToAction("Index");
        }

        
        public IActionResult DeleteCatigory(int Id)
        {
            bool success = Service.Delete(Id);
            if (success)
            {
                return RedirectToAction("Index");
            }
            return View("Error");
        }
    }
}