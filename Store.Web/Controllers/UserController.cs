﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Store.Data;
using Store.Data.Models;
using Store.Data.ViewModels;
using Store.Service;


namespace Store.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IHostingEnvironment _environment;
        //private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserController(IUserService userService, IHostingEnvironment environment, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userService = userService;
            _environment = environment;
          
            _userManager = userManager;
            _signInManager = signInManager;
        }


        #region Index 
        public async Task<IActionResult> Index(int? page, string SearchString)
        {
            var users = userService.GetUsers();
            if (SearchString != null)
            {
                users = users.Where(x => x.UserName.Contains(SearchString));
            }
            if (users.Count() <= 10) { page = 1; }
            int pageSize = 10;
            return View(await PaginatedList<User>.CreateAsync(users, page ?? 1, pageSize));
        }
        //#endregion

        //#region Add

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                userViewModel.Password = "Abc123@@";
                var user = new User { UserName = userViewModel.Email, Email = userViewModel.Email ,FirstName=userViewModel.FirstName  , LastName = userViewModel.LastName};
                var result = await _userManager.CreateAsync(user, userViewModel.Password);
                if (result.Succeeded)
                {
                    // Add a user to the default role, or any role you prefer here
                   // await _userManager.AddToRoleAsync(user, "Doctor");
                    return RedirectToAction("Index", "User");
                }
            }
            return View(userViewModel);
        }
        #endregion


        public async Task<IActionResult> Delete(string id)
        {
            bool isExist = !String.IsNullOrEmpty(id);
            if (isExist)
            {

                User usermodel = await _userManager.FindByIdAsync(id);
                if (usermodel!=null) {

                    await _userManager.DeleteAsync(usermodel);
                }
            }
            return RedirectToAction("Index");
        }
    }
    }
